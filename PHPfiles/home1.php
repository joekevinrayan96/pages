

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hotel Happy Holiday - Home</title><!--title of the website-->

<link rel="stylesheet" type="text/css" href="../CSSfiles/stylebackground.css"/><!--link to the css style of background image-->
<link rel="stylesheet" type="text/css" href="../CSSfiles/stylemenu.css" /><!--link to the css style of navigation bar-->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="../CSSfiles/modal.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/footer.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/opaque.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/slideanim.css"/>
<link rel="stylesheet" type="text/css" href="../CSSfiles/log.css"/>

<!--which contains style for opaque box-->
<!--which contains style for about border box and text-->
<!--which contains style for marquee-->
<style>


#txtother{
color: #030;
text-align:justify;
font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif; }
	
#txtblbgimg{
color: #030;
text-align:center;
font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif; }

#slide{
width:200px;
height:150px;
min-height:80px;
min-width:120px; }

.txt-inbetween{
	font-size:18px;
	color:white;
	position:absolute;
	overflow:hidden; }

</style>

</head>

<body>

	
<ul class="cb-slideshow" ><!--background image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span></li>
	<li style="list-style-type:none"><span>Image 02</span></li>
    <li style="list-style-type:none"><span>Image 03</span></li>
    <li style="list-style-type:none"><span>Image 04</span></li>
    <li style="list-style-type:none"><span>Image 05</span></li>
    <li style="list-style-type:none"><span>Image 06</span></li>
    
</ul> 

<table width=100%><!--table made to include opaque box logo and heading-->
  
  <tr>
    <td align="center">
     <!--<div class="opaque"><img src="../Images/logo.png" width="200" height="200" align="center" alt=logo/>
     <font size="+6" color="#00FF66" face="Palatino Linotype", "Book Antiqua", Palatino, serif> Hotel Happy Holiday</font>
     </div>-->
     <?php include('session.php');
   include('header.php'); ?>
    </td>
  </tr>
  
  <tr>
    <td>
    <ul class="menu">
      	<li><a href="home1.php"><font size="+1">Home</font></a></li>
  		<li><a href="accomodation1.php"><font size="+1">Accomodation</font></a></li>
  		<li><a href="dining1.php"><font size="+1">Dining</font></a></li>
  		<li><a href="packages1.php"><font size="+1">Packages</font></a></li>
  		<li><a href="location1.php"><font size="+1">Location</font></a></li>
  		<li id="contact_btnch"><a href="#contactus"><font size="+1">Contact Us</font></a></li>
  		<li><a href="home1.php"><font size="+1">About Us</font></a></li>
  		<li id="booknow_btnh" style="float:right"><a class="active" href="#booknow"><font size="+1">Book Now</font></a></li>
        </ul>
    </td>
  </tr>
  
</table>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<?php
//include('session.php');
echo '<ul class="opaquea" style="text-align:right">
	
	<li style="list-style-type:none;float:right;"><a href="Logout.php">Logout</a></li>
    <li style="list-style-type:none;float:right"><font color="#FFFFFF">'.$login_session.' &nbsp;</font></li>
</ul>';
?>
<br />

 <div>
 <section id="aboutus">
  <p> <h1 id="txtblbgimg"> Hotel Happy Holiday </h1> </p>
  <p id="txtblbgimg" style="font-size: 18px">Enjoy your holiday at the iconic Hotel Happy Holiday at the Dover beach as this beachfront property encompasses all the charm, colour and vibrancy of an idyllic paradise holiday. With the beach literaly at your doorstep you can watch the waves dance, feel the warm golden sand beneath your feet and be cooled by the breeze as the sun gently warms your skin. Hotel happy holiday delivers the quality catering to the needs of guests so that each and every guest leaves with fond memories. Leave your cares behind and come on over to our hotel, where experience after experience awaits you!
  </p>
  </section>
 </div>
  
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
   
 <table width="100%">
   <tr>
     <td width="50%"><p><h1 id="txtblbgimg" style="text-align:left">Accomodation</h1></p><p id="txtother" style="font-size:16px">Retreat to one of our 221 private guest-rooms or suites just steps away from the dazzling white sandy beach. All of our accommodations include free wi-fi, coffeemakers, mini-fridges, cable TV, air conditioning, direct dial phone, in-room safe, hairdryer, quality bathroom amenities and private balcony. Luxury suites feature jacuzzi tubs and living areas with the added option of a private bar.</p></td>
     <td width="50%">
        
        <ul class="slide-anima" ><!--image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span><div><h4>Island View</h4></div></li>
	<li style="list-style-type:none"><span>Image 02</span><div><h4>Ocean View</h4></div></li>
    <li style="list-style-type:none"><span>Image 03</span><div><h4>Pool View</h4></div></li>
    <li style="list-style-type:none"><span>Image 04</span><div><h4>State Room</h4></div></li>
    <li style="list-style-type:none"><span>Image 05</span><div><h4>Island View</h4></div></li>
    <li style="list-style-type:none"><span>Image 06</span><div><h4>Pool View</h4></div></li>
    
 
  		 
        </ul>
       
        
       
       
      </td>
     </tr>
   </table>
   
   <br />
   <table width="100%">
     <tr>
       <td width="50%">
       <ul class="slide-animd" ><!--image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span><div><h4>Accra Deck</h4></div></li>
	<li style="list-style-type:none"><span>Image 02</span><div><h4>Coco Patch</h4></div></li>
    <li style="list-style-type:none"><span>Image 03</span><div><h4>Fig Tree</h4></div></li>
    <li style="list-style-type:none"><span>Image 04</span><div><h4>Lagoon Bar</h4></div></li>
    <li style="list-style-type:none"><span>Image 05</span><div><h4>Pacifica Kitchen</h4></div></li>
    <li style="list-style-type:none"><span>Image 06</span><div><h4>Sand Bar</h4></div></li>
    
 
  		 
        </ul>
       
      
       
       </td>
	   
       <td width="50%"><p><h1 id="txtblbgimg" style="text-align:left">Dining</h1></p>
	    <p id="txtother" style="font-size:16px">
	   Enjoy an exquisite dining experience featuring local and contemporary European cuisine at Accra Deck Restaurant – a picturesque venue with outdoor seating overlooking the ocean. Get your taste buds tingling with delights of the Pacific Rim at our Pacifika Kitchen Restaurant featuring Japanese, Chinese, Thai and Indonesian cuisine. For something more casual, head to the CocoPatch restaurant for a hearty breakfast or lunch. On Tuesdays and Thursdays, immerse yourself in the local culture at our specialty dinners with live music and entertainment at The Fig Tree Restaurant. Thirsty? Grab an ice cold drink at the Lagoon Bar, our swim-up pool bar. This laid-back hangout has an enticing snack bar menu for poolside bites and hosts karaoke every Wednesday evening. We also have the Sand Bar, an indoor bar/lounge with a big-screen TV for watching your favorite sports games or enjoy cocktails as the sun set at our newly established Sunset Bar. Don’t feel like coming down to dine? Our in room dining is available daily from 7.00 a.m. to 11.00 p.m.
	    </p>
	   </td>
     </tr>
   </table>
   <br />
   <table width="100%">
     <tr>
       <td width="50%"><p><h1 id="txtblbgimg" style="text-align:left">Tour Packages</h1></p>
       <p id="txtother" style="font-size:16px">
       Enjoy site seeing the amazing place Dover, taken for a tour from our Hotel Happy Holiday. The packages include visits to the wondrous places of Dover. To the Dover museum, to the wild life wingham park to the kearnsney abbey botanical garden and site see the beautiful Dover town in Kent and much more. We will provide you with transport and buffet will be following where you can enjoy your meals at our partner restaurant nearby the location and a guide whom will guide you throughout the tour. All these inclusive 
in the packages for a reasonable rate.
       </p>
       </td>
       <td width="50%">
      <ul class="slide-animp" ><!--image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span><div><h4>Dover Museum</h4></div></li>
	<li style="list-style-type:none"><span>Image 02</span><div><h4>Kearnsney Abbey</h4></div></li>
    <li style="list-style-type:none"><span>Image 03</span><div><h4>Wingham Park</h4></div></li>
    <li style="list-style-type:none"><span>Image 04</span><div><h4>Dover Museum</h4></div></li>
    <li style="list-style-type:none"><span>Image 05</span><div><h4>Kearnsney Abbey</h4></div></li>
    <li style="list-style-type:none"><span>Image 06</span><div><h4>Wingham Park</h4></div></li>
    
 
  		 
        </ul>
       
       
       </td>
     </tr>
   </table>
   <p>&nbsp;</p>
   
   
   <!-- The Modal -->
   <div>
<!--<divm id="myModala" class="modal">

  <!-- Modal content 
  <div class="modal-content">
    <span class="close">&times;</span>
    <form name="contactusfa" method="post" action="home.html" onsubmit="return formValidationa();">
    <h1 style="text-align:center">Contact Us</h1>
<p style="font-size:18px">Name</p>
<input name="fullnamea" type="text" id="fullnamea" placeholder="Your Name"/>
<p style="font-size:18px">Email</p>
<input name="emaila" type="text" id="emaila" placeholder="Your email address"/>
<p style="font-size:18px">Inquiry</p>
<textarea name="inquirya" rows="5"   type="text" id="inquirya" placeholder="Your inquiry"></textarea>

<p>
  <input type="submit" name="btnsubmita" id="btnsubmita" value="Submit" />
  
</p>

<p style="font-size:12px">For more details call: 011-2451932</p>

</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var modala = document.getElementById('myModala');

// Get the button that opens the modal
var btna = document.getElementById("contactusa");

// Get the <span> element that closes the modal
var spana = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btna.onclick = function() {
    modala.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
spana.onclick = function() {
    modala.style.display = "none";
}

function formValidationa()
{
	var namea=document.contactusfa.fullnamea;
	var emaila=document.contactusfa.emaila;
	var inquirya=document.contactusfa.inquirya;
	
	if(EmptyValidationa(namea,emaila,inquirya))
	{
		if(AllLettersa(namea))
		{
			if(Emaila(emaila))
			{
				alert("Your inquiry is sent successfully");
				return true;
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationa(namea,emaila,inquirya)
{
	var namea_lena=namea.value.length;
	var emaila_lena=emaila.value.length;
	var inquirya_lena=inquirya.value.length;
	
	if(namea_lena==0||emaila_lena==0||inquirya_lena==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersa(namea)
{
	var lettersa=/^[A-Za-z]+$/;
	if(namea.value.match(lettersa))
	{
		return true;
	}
	else
	{
		alert("Name should contain only alphabets");
		namea.focus();
		return false;
	}
}


function Emaila(emaila)
{
	var lettera=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emaila.value.match(lettera))
	{
		return true;
	}
	else
	{
		alert("Invalid email format");
		emaila.focus();
		return false;
	}
}


</script>-->
<?php include('contactmodalh.php');?>

   
<!-- The Modal -->

<?php include('booknowmodalh.php');?>
<!--<divm id="myModalb" class="modal">

  <!-- Modal content
  <div class="modal-content">
    <span class="closeb">&times;</span>
    <form name="booknowfb" method="post" action="home.html" onsubmit="return formValidationb();">
    <h1 style="text-align:center">Book Now</h1>
<p style="font-size:18px">First Name</p>
<input name="firstnameb" type="text" id="firstnameb" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="lastnameb" type="text" id="lastnameb" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="emailb" type="text" id="emailb" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="mobileb" type="text" id="mobileb" placeholder="Your mobile No." />
<p style="font-size:18px">Check in</p>
<input name="checkinb" type="date" id="checkinb" />
<p style="font-size:18px">Check out</p>
<input name="checkoutb" type="date" id="checkoutb" />
<p style="font-size:18px">Select type of room</p>
<input list="roomsb" name="roomsib" id="roomsib" type="text"/>
<datalist id="roomsb">
    <option value="Island View">
    <option value="Ocean View">
    <option value="Pool View">
    <option value="State Room">
</datalist>

<p>
  <input type="submit" name="btnsubmitb" id="btnsubmitb" value="Submit" />
  
</p>



</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var modalb = document.getElementById('myModalb');

// Get the button that opens the modal
var btnb = document.getElementById("booknowb");

// Get the <span> element that closes the modal
var spanb = document.getElementsByClassName("closeb")[0];

// When the user clicks the button, open the modal 
btnb.onclick = function() {
    modalb.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
spanb.onclick = function() {
    modalb.style.display = "none";
}

function formValidationb()
{
	var fnameb=document.booknowfb.firstnameb;
	var lnameb=document.booknowfb.lastnameb;
	var emailb=document.booknowfb.emailb;
	var contactnob=document.booknowfb.mobileb;
	var checkinb=document.booknowfb.checkinb;
	var checkoutb=document.booknowfb.checkoutb;
	var roomb=document.booknowfb.roomsib;
	
	if(EmptyValidationb(fnameb,lnameb,emailb,contactnob,checkinb,checkoutb,roomb))
	{
		if(AllLettersb(fnameb,lnameb))
		{
			if(Emailb(emailb))
			{
				if(Allnumericb(contactnob))
				{
					if(!checkinb.getText().toString().equals("mm/dd/yyyy"))
					{
						if(!checkoutb.getText().toString().equals("mm/dd/yyyy"))
						{
							alert("Booking Details successfully sent!");
							return true;
						}
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationb(fnameb,lnameb,emailb,contactnob,checkinb,checkoutb,roomb)
{
	var fnameb_lenb=fnameb.value.length;
	var lnameb_lenb=lnameb.value.length;
	var emailab_lenb=emailb.value.length;
	var contactnob_lenb=contactnob.value.length;
	var checkinb_lenb=checkinb.value.length;
	var checkoutb_lenb=checkoutb.value.length;
	var roomb_lenb=roomb.value.length;
	
	if(fnameb_lenb==0||lnameb_lenb==0||emailab_lenb==0||contactnob_lenb==0||checkinb_lenb==0||checkoutb_lenb==0||roomb_lenb==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersb(fnameb,lnameb)
{
	var lettersb=/^[A-Za-z]+$/;
	if(fnameb.value.match(lettersb)&&lnameb.value.match(lettersb))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		fnameb.focus();
		return false;
	}
}


function Emailb(emailb)
{
	var letterb=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailb.value.match(letterb))
	{
		return true;
	}
	else
	{
		alert("Invalid email format")
		emailb.focus();
		return false;
	}
}

function Allnumericb(contactnob)
{
	var lettersnb=/^[0-9]+$/;
	if(contactnob.value.match(lettersnb))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobileb.focus();
		return false;
	}
}




</script>-->

</div>
   
   
   
   <!--<footer>
     <table width="100%">
       <tr>
         <td width="33.33%"><img src="../Images/logo.png"/></td>
         <td width="33.33%">
         
         <p style="font-size:16px">
         <i class="material-icons" style="font-size:24pxpx; color:#0FF; text-shadow:2px 2px 4px #000000;">phone</i>&nbsp;  Tel:0112451932
         </p>
         <p>
         <i class="material-icons" style="font-size:24px; color:#0FF; text-shadow:2px 2px 4px #000000;">home</i>&nbsp;75, Folkestone Road, Dover, CT17 9RZ, United Kingdom</p>
         <p><i class="material-icons" style="font-size:24px; color:#0FF; text-shadow:2px 2px 4px #000000;">email</i>&nbsp;      hotelhappyholidaydover@gmail.com</p>
         </td>
         <td width="33.33%" style="text-align:center">
         
         <h3>Like us on</h3>
         <p><a href="https://www.facebook.com/"><img src="../Images/facebook_icon.png" style="width:50px; height:50px;"/></a></p>
         </td>
       </tr>
     </table>
   
</footer>-->
<?php include('footer.php'); ?>
     
     
     
     
</body>

</html>
