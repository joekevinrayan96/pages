<?php include('session.php'); ?>
<divm id="booknowh" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closeb">&times;</span>
    <form name="booknow_formh" method="post" action="home1.html" onsubmit="return validate_booknowh();">
    <h1 style="text-align:center">Book Now</h1>
<p style="font-size:18px">First Name</p>
<input name="txt_firstnamebh" type="text" id="txt_firstnamebh" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="txt_lastnamebh" type="text" id="txt_lastnamebh" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="txt_emailbh" type="text" id="txt_emailbh" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="txt_mobilebh" type="text" id="txt_mobilebh" placeholder="Your mobile No." />
<p style="font-size:18px">Check in</p>
<input name="date_checkinbh" type="date" id="date_checkinbh" />
<p style="font-size:18px">Check out</p>
<input name="date_checkoutbh" type="date" id="date_checkoutbh" />
<p style="font-size:18px">Select type of room</p>
<input list="list_roomsbh" name="cmb_roomsibh" id="cmb_roomsibh" type="text"/>
<datalist id="list_roomsbh">
    <option value="Island View">
    <option value="Ocean View">
    <option value="Pool View">
    <option value="State Room">
</datalist>

<p>
  <input type="submit" name="booknow_btn_submith" id="booknow_btn_submith" value="Submit" />
</p>
</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var booknowmodalh = document.getElementById('booknowh');

// Get the button that opens the modal
var booknowbtnh = document.getElementById("booknow_btnh");

// Get the <span> element that closes the modal
var booknowspanh = document.getElementsByClassName("closeb")[0];

// When the user clicks the button, open the modal 
booknowbtnh.onclick = function() {
    booknowmodalh.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
booknowspanh.onclick = function() {
    booknowmodalh.style.display = "none";
}

function validate_booknowh()
{
	var firstnamebh=document.booknow_formh.txt_firstnamebh;
	var lastnamebh=document.booknow_formh.txt_lastnamebh;
	var emailbh=document.booknow_formh.txt_emailbh;
	var contactnobh=document.booknow_formh.txt_mobilebh;
	var checkinbh=document.booknow_formh.date_checkinbh;
	var checkoutbh=document.booknow_formh.date_checkoutbh;
	var roombh=document.booknow_formh.cmb_roomsibh;
	
	if(EmptyValidationbooknowh(firstnamebh,lastnamebh,emailbh,contactnobh,checkinbh,checkoutbh,roombh))
	{
		if(AllLettersbooknowh(firstnamebh,lastnamebh))
		{
			if(Emailbooknowh(emailbh))
			{
				if(Allnumericbooknowh(contactnobh))
				{
					if(!checkinbh.getText().toString().equals("mm/dd/yyyy"))
					{
						if(!checkoutbh.getText().toString().equals("mm/dd/yyyy"))
						{
							alert("Booking Details successfully sent!");
							return true;
						}
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationbooknowh(firstnamebh,lastnamebh,emailbh,contactnobh,checkinbh,checkoutbh,roombh)
{
	var firstname_lengthbh=firstnamebh.value.length;
	var lastname_lengthbh=lastnamebh.value.length;
	var email_lengthbh=emailbh.value.length;
	var contactno_lengthbh=contactnobh.value.length;
	var checkin_lengthbh=checkinbh.value.length;
	var checkout_lengthbh=checkoutbh.value.length;
	var room_lengthbh=roombh.value.length;
	
	if(firstname_lengthbh==0||lastname_lengthbh==0||email_lengthbh==0||contactno_lengthbh==0||checkin_lengthbh==0||checkout_lengthbh==0||room_lengthbh==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersbooknowh(firstnamebh,lastnamebh)
{
	var lettersbh=/^[A-Za-z]+$/;
	if(firstnamebh.value.match(lettersbh)&&lastnamebh.value.match(lettersbh))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		firstnamebh.focus();
		return false;
	}
}


function Emailbooknowh(emailbh)
{
	var letterbh=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailbh.value.match(letterbh))
	{
		return true;
	}
	else
	{
		alert("Invalid email format")
		emailbh.focus();
		return false;
	}
}

function Allnumericbooknowh(contactnobh)
{
	var lettersnbh=/^[0-9]+$/;
	if(contactnobh.value.match(lettersnbh))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobilebh.focus();
		return false;
	}
}

</script>

<?php
if(isset($_POST['booknow_btn_submith']))
{
include('connection.php');

$firstnamebh=$_POST['txt_firstnamebh'];
$lastnamebh=$_POST['txt_lastnamebh'];
$emailbh=$_POST['txt_emailbh'];
$mobilebh=$_POST['txt_mobilebh'];
$checkinbh=$_POST['date_checkinbh'];
$checkoutbh=$_POST['date_checkoutbh'];
$roombh=$_POST['cmb_roomsibh'];

$sqlbh="insert into tbl_bookdetails values('$firstnamebh','$lastnamebh','$emailbh','$mobilebh','$checkinbh','$checkoutbh','$roombh','$login_session',DEFAULT)";
$databh=mysqli_query($conn,$sqlbh);
if($databh)
{
echo "You bookings have been saved! Soon we will contact you! Thank you!";
}
else
{
die('could not enter data'.mysqli_error());
}
}

?>
