<?php include('session.php'); ?>
<divm id="booknowa" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closeb">&times;</span>
    <form name="booknow_forma" method="post" action="home.html" onSubmit="return validate_booknowa();">
    <h1 style="text-align:center">Book Now</h1>
<p style="font-size:18px">First Name</p>
<input name="txt_firstnameba" type="text" id="txt_firstnameba" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="txt_lastnameba" type="text" id="txt_lastnameba" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="txt_emailba" type="text" id="txt_emailba" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="txt_mobileba" type="text" id="txt_mobileba" placeholder="Your mobile No." />
<p style="font-size:18px">Check in</p>
<input name="date_checkinba" type="date" id="date_checkinba" />
<p style="font-size:18px">Check out</p>
<input name="date_checkoutba" type="date" id="date_checkoutba" />
<p style="font-size:18px">Select type of room</p>
<input list="list_roomsba" name="cmb_roomsiba" id="cmb_roomsiba" type="text"/>
<datalist id="list_roomsba">
    <option value="Island View">
    <option value="Ocean View">
    <option value="Pool View">
    <option value="State Room">
</datalist>

<p>
  <input type="submit" name="booknow_btn_submita" id="booknow_btn_submita" value="Submit" />
</p>
</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var booknowmodala = document.getElementById('booknowa');

// Get the button that opens the modal
var booknowbtna = document.getElementById("booknow_btna");

// Get the <span> element that closes the modal
var booknowspana = document.getElementsByClassName("closeb")[0];

// When the user clicks the button, open the modal 
booknowbtna.onclick = function() {
    booknowmodala.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
booknowspana.onclick = function() {
    booknowmodala.style.display = "none";
}

function validate_booknowa()
{
	var firstnameba=document.booknow_forma.txt_firstnameba;
	var lastnameba=document.booknow_forma.txt_lastnameba;
	var emailba=document.booknow_forma.txt_emailba;
	var contactnoba=document.booknow_forma.txt_mobileba;
	var checkinba=document.booknow_forma.date_checkinba;
	var checkoutba=document.booknow_forma.date_checkoutba;
	var roomba=document.booknow_forma.cmb_roomsiba;
	
	if(EmptyValidationbooknowa(firstnameba,lastnameba,emailba,contactnoba,checkinba,checkoutba,roomba))
	{
		if(AllLettersbooknowa(firstnameba,lastnameba))
		{
			if(Emailbooknowa(emailba))
			{
				if(Allnumericbooknowa(contactnoba))
				{
					if(!checkinba.getText().toString().equals("mm/dd/yyyy"))
					{
						if(!checkoutba.getText().toString().equals("mm/dd/yyyy"))
						{
							alert("Booking Details successfully sent!");
							return true;
						}
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationbooknowa(firstnameba,lastnameba,emailba,contactnoba,checkinba,checkoutba,roomba)
{
	var firstname_lengthba=firstnameba.value.length;
	var lastname_lengthba=lastnameba.value.length;
	var email_lengthba=emailba.value.length;
	var contactno_lengthba=contactnoba.value.length;
	var checkin_lengthba=checkinba.value.length;
	var checkout_lengthba=checkoutba.value.length;
	var room_lengthba=roomba.value.length;
	
	if(firstname_lengthba==0||lastname_lengthba==0||email_lengthba==0||contactno_lengthba==0||checkin_lengthba==0||checkout_lengthba==0||room_lengthba==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersbooknowa(firstnameba,lastnameba)
{
	var lettersba=/^[A-Za-z]+$/;
	if(firstnameba.value.match(lettersba)&&lastnameba.value.match(lettersba))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		firstnameba.focus();
		return false;
	}
}


function Emailbooknowa(emailba)
{
	var letterba=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailba.value.match(letterba))
	{
		return true;
	}
	else
	{
		alert("Invalid email format")
		emailba.focus();
		return false;
	}
}

function Allnumericbooknowa(contactnoba)
{
	var lettersnba=/^[0-9]+$/;
	if(contactnoba.value.match(lettersnba))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobileba.focus();
		return false;
	}
}

</script>

<?php
if(isset($_POST['booknow_btn_submita']))
{
include('connection.php');

$firstnameba=$_POST['txt_firstnameba'];
$lastnameba=$_POST['txt_lastnameba'];
$emailba=$_POST['txt_emailba'];
$mobileba=$_POST['txt_mobileba'];
$checkinba=$_POST['date_checkinba'];
$checkoutba=$_POST['date_checkoutba'];
$roomba=$_POST['cmb_roomsiba'];

$sqlba="insert into tbl_bookdetails values('$firstnameba','$lastnameba','$emailba','$mobileba','$checkinba','$checkoutba','$roomba','$login_session')";
$databa=mysqli_query($conn,$sqlba);
if($databa)
{
echo "You bookings have been saved! Soon we will contact you! Thank you!";
}
else
{
die('could not enter data'.mysqli_error());
}
}

?>
