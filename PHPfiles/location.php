<!DOCTYPE html>
<html>
  <head>
  <title>Hotel Happy Holiday - Location</title>
  <link rel="stylesheet" type="text/css" href="../CSSfiles/stylebackground.css"/><!--link to the css style of background image-->
<link rel="stylesheet" type="text/css" href="../CSSfiles/stylemenu.css" /><!--link to the css style of navigation bar-->
<link rel="stylesheet" type="text/css" href="../CSSfiles/opaque.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/footer.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="../CSSfiles/modal.css" />
    <style>
       #map {
        height: 400px;
        width: 100%;
       }
	   
	   
	   #txtother{
color: #030;
text-align:justify;
font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif; }

    </style>
  </head>
  <body>
  <table width=100%><!--table made to include opaque box logo and heading-->
  
  <tr>
    <td align="center">
     <div class="opaque"><img src="../Images/logo.png" width="200" height="200" align="center" alt=logo/>
     <font size="+6" color="#00FF66" face="Palatino Linotype", "Book Antiqua", Palatino, serif> Hotel Happy Holiday</font>
     </div>
    </td>
  </tr>
  
  <tr>
    <td>
      <ul class="menu">
      	<li><a href="home.php"><font size="+1">Home</font></a></li>
  		<li><a href="accomodation.php"><font size="+1">Accomodation</font></a></li>
  		<li><a href="dining.php"><font size="+1">Dining</font></a></li>
  		<li><a href="packages.php"><font size="+1">Packages</font></a></li>
  		<li><a href="location.php"><font size="+1">Location</font></a></li>
  		<li id="login_btncl"><a href="#contactus"><font size="+1">Contact Us</font></a></li>
  		<li><a href="home.php"><font size="+1">About Us</font></a></li>
  		<li id="login_btnbl" style="float:right"><a class="active" href="#booknow"><font size="+1">Book Now</font></a></li>
        </ul>
    </td>
  </tr>
  
</table>

<ul class="opaquea" style="text-align:right">
	
	<li id="login_btnl" style="list-style-type:none;float:right;"><font color="#FFFFFF"><a href="#login">Login</a></font></li>
    <li style="list-style-type:none;float:right"><font color="#FFFFFF">Guest &nbsp;</font></li>
</ul>

  <div id="txtother">
    <h1 style="font-size:36px">Our Location</h1>
    
    <p style="font-size:16px">Our Hotel is situated at the coastal ranges of dover where important places like Museum, Botanical gardens and Wild Life parks are situated nearby. Which makes our hotel a splendid location to enjoy your holiday</p>
    </div>
    <div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: 51.125760, lng:1.307362};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWgVJrayp9f-uX-XLX-cW9j_BDS9ZzGaQ&callback=initMap">
    </script>
    <br>
    <br>
    
<div>

<?php include('loginmodalp.php');?>
<?php include('registermodalp.php');?>

</div>

<?php include('footer.php'); ?>
    
  </body>
</html>