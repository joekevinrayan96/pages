<?php include('session.php'); ?>
<divm id="booknowp" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closeb">&times;</span>
    <form name="booknow_formp" method="post" action="home.html" onsubmit="return validate_booknowp();">
    <h1 style="text-align:center">Book Now</h1>
<p style="font-size:18px">First Name</p>
<input name="txt_firstnamebp" type="text" id="txt_firstnamebp" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="txt_lastnamebp" type="text" id="txt_lastnamebp" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="txt_emailbp" type="text" id="txt_emailbp" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="txt_mobilebp" type="text" id="txt_mobilebp" placeholder="Your mobile No." />
<p style="font-size:18px">Check in</p>
<input name="date_checkinbp" type="date" id="date_checkinbp" />
<p style="font-size:18px">Check out</p>
<input name="date_checkoutbp" type="date" id="date_checkoutbp" />
<p style="font-size:18px">Select type of room</p>
<input list="list_roomsbp" name="cmb_roomsibp" id="cmb_roomsibp" type="text"/>
<datalist id="list_roomsbp">
    <option value="Island View">
    <option value="Ocean View">
    <option value="Pool View">
    <option value="State Room">
</datalist>

<p>
  <input type="submit" name="booknow_btn_submitp" id="booknow_btn_submitp" value="Submit" />
</p>
</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var booknowmodalp = document.getElementById('booknowp');

// Get the button that opens the modal
var booknowbtnp = document.getElementById("booknow_btnp");

// Get the <span> element that closes the modal
var booknowspanp = document.getElementsByClassName("closeb")[0];

// When the user clicks the button, open the modal 
booknowbtnp.onclick = function() {
    booknowmodalp.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
booknowspanp.onclick = function() {
    booknowmodalp.style.display = "none";
}

function validate_booknowp()
{
	var firstnamebp=document.booknow_formp.txt_firstnamebp;
	var lastnamebp=document.booknow_formp.txt_lastnamebp;
	var emailbp=document.booknow_formp.txt_emailbp;
	var contactnobp=document.booknow_formp.txt_mobilebp;
	var checkinbp=document.booknow_formp.date_checkinbp;
	var checkoutbp=document.booknow_formp.date_checkoutbp;
	var roombp=document.booknow_formp.cmb_roomsibp;
	
	if(EmptyValidationbooknowp(firstnamebp,lastnamebp,emailbp,contactnobp,checkinbp,checkoutbp,roombp))
	{
		if(AllLettersbooknowp(firstnamebp,lastnamebp))
		{
			if(Emailbooknowp(emailbp))
			{
				if(Allnumericbooknowp(contactnobp))
				{
					if(!checkinbp.getText().toString().equals("mm/dd/yyyy"))
					{
						if(!checkoutbp.getText().toString().equals("mm/dd/yyyy"))
						{
							alert("Booking Details successfully sent!");
							return true;
						}
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationbooknowp(firstnamebp,lastnamebp,emailbp,contactnobp,checkinbp,checkoutbp,roombp)
{
	var firstname_lengthbp=firstnamebp.value.length;
	var lastname_lengthbp=lastnamebp.value.length;
	var email_lengthbp=emailbp.value.length;
	var contactno_lengthbp=contactnobp.value.length;
	var checkin_lengthbp=checkinbp.value.length;
	var checkout_lengthbp=checkoutbp.value.length;
	var room_lengthbp=roombp.value.length;
	
	if(firstname_lengthbp==0||lastname_lengthbp==0||email_lengthbp==0||contactno_lengthbp==0||checkin_lengthbp==0||checkout_lengthbp==0||room_lengthbp==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersbooknowp(firstnamebp,lastnamebp)
{
	var lettersbp=/^[A-Za-z]+$/;
	if(firstnamebp.value.match(lettersbp)&&lastnamebp.value.match(lettersbp))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		firstnamebp.focus();
		return false;
	}
}


function Emailbooknowp(emailbp)
{
	var letterbp=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailbp.value.match(letterbp))
	{
		return true;
	}
	else
	{
		alert("Invalid email format")
		emailbp.focus();
		return false;
	}
}

function Allnumericbooknowp(contactnobp)
{
	var lettersnbp=/^[0-9]+$/;
	if(contactnobp.value.match(lettersnbp))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobilebp.focus();
		return false;
	}
}

</script>

<?php
if(isset($_POST['booknow_btn_submitp']))
{
include('connection.php');

$firstnamebp=$_POST['txt_firstnamebp'];
$lastnamebp=$_POST['txt_lastnamebp'];
$emailbp=$_POST['txt_emailbp'];
$mobilebp=$_POST['txt_mobilebp'];
$checkinbp=$_POST['date_checkinbp'];
$checkoutbp=$_POST['date_checkoutbp'];
$roombp=$_POST['cmb_roomsibp'];

$sqlbp="insert into tbl_bookdetails values('$firstnamebp','$lastnamebp','$emailbp','$mobilebp','$checkinbp','$checkoutbp','$roombp','$login_session')";
$databp=mysqli_query($conn,$sqlbp);
if($databp)
{
echo "You bookings have been saved! Soon we will contact you! Thank you!";
}
else
{
die('could not enter data'.mysqli_error());
}
}

?>
