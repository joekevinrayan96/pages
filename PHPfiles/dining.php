<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hotel Happy Holiday - Dining</title>
<link rel="stylesheet" type="text/css" href="../CSSfiles/opaque.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/stylemenu.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/modal.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/footer.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="../CSSfiles/dinebackground.css" />

<style>


#txtother{
color: #030;
text-align:justify;
font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif; }

</style>

</head>

<body>

<ul class="cb-slideshowd" ><!--background image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span></li>
	<li style="list-style-type:none"><span>Image 02</span></li>
    <li style="list-style-type:none"><span>Image 03</span></li>
    <li style="list-style-type:none"><span>Image 04</span></li>
    <li style="list-style-type:none"><span>Image 05</span></li>
    <li style="list-style-type:none"><span>Image 06</span></li>
    
</ul> 

<table width=100%><!--table made to include opaque box logo and heading-->
  
  <tr>
    <td align="center">
     <div class="opaque"><img src="../Images/logo.png" width="200" height="200" align="center" alt=logo/>
     <font size="+6" color="#00FF66" face="Palatino Linotype", "Book Antiqua", Palatino, serif> Hotel Happy Holiday</font>
     </div>
    </td>
  </tr>
  
  <tr>
    <td>
     <ul class="menu">
      	<li><a href="home.php"><font size="+1">Home</font></a></li>
  		<li><a href="accomodation.php"><font size="+1">Accomodation</font></a></li>
  		<li><a href="dining.php"><font size="+1">Dining</font></a></li>
  		<li><a href="packages.php"><font size="+1">Packages</font></a></li>
  		<li><a href="location.php"><font size="+1">Location</font></a></li>
  		<li id="login_btncd"><a href="#contactus"><font size="+1">Contact Us</font></a></li>
  		<li><a href="home.php"><font size="+1">About Us</font></a></li>
  		<li id="login_btnbd" style="float:right"><a class="active" href="#booknow"><font size="+1">Book Now</font></a></li>
        </ul>
    </td>
  </tr>
  
</table>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<ul class="opaquea" style="text-align:right">
	
	<li id="login_btnd" style="list-style-type:none;float:right;"><font color="#FFFFFF"><a href="#login">Login</a></font></li>
    <li style="list-style-type:none;float:right"><font color="#FFFFFF">Guest &nbsp;</font></li>
</ul>

<div style="border-bottom-color:#F00; border-bottom:groove;">
<p><h1 id="txtother" style="font-size:48px; ">Dining</h1></p>
<p id="txtother" style="text-align:justify">Enjoy an exquisite dining experience featuring local and contemporary European cuisine at Accra Deck Restaurant – a picturesque venue with outdoor seating overlooking the ocean. Get your taste buds tingling with delights of the Pacific Rim at our Pacifika Kitchen Restaurant featuring Japanese, Chinese, Thai and Indonesian cuisine. For something more casual, head to the CocoPatch restaurant for a hearty breakfast or lunch. On Tuesdays and Thursdays, immerse yourself in the local culture at our specialty dinners with live music and entertainment at The Fig Tree Restaurant. Thirsty? Grab an ice cold drink at the Lagoon Bar, our swim-up pool bar. This laid-back hangout has an enticing snack bar menu for poolside bites and hosts karaoke every Wednesday evening. We also have the Sand Bar, an indoor bar/lounge with a big-screen TV for watching your favorite sports games or enjoy cocktails as the sun set at our newly established Sunset Bar. Don’t feel like coming down to dine? Our in room dining is available daily from 7.00 a.m. to 11.00 p.m.
&nbsp;

	
</p>
</div>

<div>
<p><h1 id="txtother">Accra Deck</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Dining/accra_deck.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-bottom">Exquisite open-air, ocean-front dining featuring local and contemporary European cuisine and a fine selection of wines. 
        
    </p>
    <p id="txtother" style="font-size:14px">Open hours: Daily 6.00a.m to 11.00a.m
    
    </p></td>
  </tr>
</table>

</p>
</div>

<div>
<p><h1 id="txtother">Pacifika Kitchen</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Dining/pacifica_kitchen.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-bottom">Pacifika Kitchen is a dazzling one-of-a-kind restaurant on the island. Enjoy Japanese, Chinese, Thai and Indonesian cuisine in our modern indoor dining venue.
        
    </p>
    <p id="txtother" style="font-size:14px">Open hours: Daily 12.00p.m to 6.00p.m
    
    </p></td>
  </tr>
</table>

</p>
</div>

<div>
<p><h1 id="txtother">Coco Patch</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Dining/coco_patch.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-bottom">The CocoPatch Restaurant is a Caribbean-style restaurant that serves breakfast and lunch daily, as well as a sumptuous local and international lunch buffet every Sunday. 
        
    </p>
    <p id="txtother" style="font-size:14px">Open hours: Daily 4.00p.m to 11.00p.m
    
    </p></td>
  </tr>
</table>

</p>
</div>

<div>
<p><h1 id="txtother">Fig Tree</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Dining/fig_tree.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-bottom">The Fig Tree Restaurant is an outdoor, oceanfront venue for themed events. On Tuesday, we host a Curry and Kebab Night accompanied by live entertainment.
        
    </p>
    <p id="txtother" style="font-size:14px">Open hours: Daily 3.00p.m to 11.00p.m
    
    </p></td>
  </tr>
</table>

</p>
</div>

<div>
<p><h1 id="txtother">Lagoon Bar</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Dining/lagoon_bar.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-bottom">This action packed bar features amphitheater seating and a swim-up bar.
        
    </p>
    <p id="txtother" style="font-size:14px">Open hours: Daily 6.00p.m to 12.30p.m
    
    </p></td>
  </tr>
</table>

</p>
</div>

<div>
<p><h1 id="txtother">Sand Bar</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Dining/sand_bar.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-bottom">Located next to the lobby, the Sand Bar serves specialty coffees, wine, beers, cocktails, snacks and screens live sporting events.
        
    </p>
    <p id="txtother" style="font-size:14px">Open hours: Daily 7.00p.m to 3.00a.m
    
    </p></td>
  </tr>
</table>

</p>
</div>

<div>
<p><h1 id="txtother">Sunset bar</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Dining/sunset_bar.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-bottom">This ideal beachfront location is the perfect place to sip cocktails and watch the sun go down.
        
    </p>
    <p id="txtother" style="font-size:14px">Open hours: Daily 5.30p.m to 6.00a.m
    
    </p></td>
  </tr>
</table>

</p>
</div>

&nbsp;

<p id="txtother" style="font-size:18px">For quick reservation please call 0112451932</p>

<br />
<br />

<div>

<?php include('loginmodald.php');?>
<?php include('registermodald.php');?>

</div>

   
   <?php include('footer.php'); ?>
   
   </body>
</html>
