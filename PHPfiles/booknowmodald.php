<?php include('session.php'); ?>
<divm id="booknowd" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closeb">&times;</span>
    <form name="booknow_formd" method="post" action="home.html" onSubmit="return validate_booknowd();">
    <h1 style="text-align:center">Book Now</h1>
<p style="font-size:18px">First Name</p>
<input name="txt_firstnamebd" type="text" id="txt_firstnamebd" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="txt_lastnamebd" type="text" id="txt_lastnamebd" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="txt_emailbd" type="text" id="txt_emailbd" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="txt_mobilebd" type="text" id="txt_mobilebd" placeholder="Your mobile No." />
<p style="font-size:18px">Check in</p>
<input name="date_checkinbd" type="date" id="date_checkinbd" />
<p style="font-size:18px">Check out</p>
<input name="date_checkoutbd" type="date" id="date_checkoutbd" />
<p style="font-size:18px">Select type of room</p>
<input list="list_roomsbd" name="cmb_roomsibd" id="cmb_roomsibd" type="text"/>
<datalist id="list_roomsbd">
    <option value="Island View">
    <option value="Ocean View">
    <option value="Pool View">
    <option value="State Room">
</datalist>

<p>
  <input type="submit" name="booknow_btn_submitd" id="booknow_btn_submitd" value="Submit" />
</p>
</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var booknowmodald = document.getElementById('booknowd');

// Get the button that opens the modal
var booknowbtnd = document.getElementById("booknow_btnd");

// Get the <span> element that closes the modal
var booknowspand = document.getElementsByClassName("closeb")[0];

// When the user clicks the button, open the modal 
booknowbtnd.onclick = function() {
    booknowmodald.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
booknowspand.onclick = function() {
    booknowmodald.style.display = "none";
}

function validate_booknowd()
{
	var firstnamebd=document.booknow_formd.txt_firstnamebd;
	var lastnamebd=document.booknow_formd.txt_lastnamebd;
	var emailbd=document.booknow_formd.txt_emailbd;
	var contactnobd=document.booknow_formd.txt_mobilebd;
	var checkinbd=document.booknow_formd.date_checkinbd;
	var checkoutbd=document.booknow_formd.date_checkoutbd;
	var roombd=document.booknow_formd.cmb_roomsibd;
	
	if(EmptyValidationbooknowd(firstnamebd,lastnamebd,emailbd,contactnobd,checkinbd,checkoutbd,roombd))
	{
		if(AllLettersbooknowd(firstnamebd,lastnamebd))
		{
			if(Emailbooknowd(emailbd))
			{
				if(Allnumericbooknowd(contactnobd))
				{
					if(!checkinbd.getText().toString().equals("mm/dd/yyyy"))
					{
						if(!checkoutbd.getText().toString().equals("mm/dd/yyyy"))
						{
							alert("Booking Details successfully sent!");
							return true;
						}
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationbooknowd(firstnamebd,lastnamebd,emailbd,contactnobd,checkinbd,checkoutbd,roombd)
{
	var firstname_lengthbd=firstnamebd.value.length;
	var lastname_lengthbd=lastnamebd.value.length;
	var email_lengthbd=emailbd.value.length;
	var contactno_lengthbd=contactnobd.value.length;
	var checkin_lengthbd=checkinbd.value.length;
	var checkout_lengthbd=checkoutbd.value.length;
	var room_lengthbd=roombd.value.length;
	
	if(firstname_lengthbd==0||lastname_lengthbd==0||email_lengthbd==0||contactno_lengthbd==0||checkin_lengthbd==0||checkout_lengthbd==0||room_lengthbd==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersbooknowd(firstnamebd,lastnamebd)
{
	var lettersbd=/^[A-Za-z]+$/;
	if(firstnamebd.value.match(lettersbd)&&lastnamebd.value.match(lettersbd))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		firstnamebd.focus();
		return false;
	}
}


function Emailbooknowd(emailbd)
{
	var letterbd=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailbd.value.match(letterbd))
	{
		return true;
	}
	else
	{
		alert("Invalid email format")
		emailbd.focus();
		return false;
	}
}

function Allnumericbooknowd(contactnobd)
{
	var lettersnbd=/^[0-9]+$/;
	if(contactnobd.value.match(lettersnbd))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobilebd.focus();
		return false;
	}
}

</script>

<?php
if(isset($_POST['booknow_btn_submitd']))
{
include('connection.php');

$firstnamebd=$_POST['txt_firstnamebd'];
$lastnamebd=$_POST['txt_lastnamebd'];
$emailbd=$_POST['txt_emailbd'];
$mobilebd=$_POST['txt_mobilebd'];
$checkinbd=$_POST['date_checkinbd'];
$checkoutbd=$_POST['date_checkoutbd'];
$roombd=$_POST['cmb_roomsibd'];

$sqlbd="insert into tbl_bookdetails values('$firstnamebd','$lastnamebd','$emailbd','$mobilebd','$checkinbd','$checkoutbd','$roombd','$login_session')";
$databd=mysqli_query($conn,$sqlbd);
if($databd)
{
echo "You bookings have been saved! Soon we will contact you! Thank you!";
}
else
{
die('could not enter data'.mysqli_error());
}
}

?>
