<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hotel Happy Holiday - Packages</title>
<link rel="stylesheet" type="text/css" href="../CSSfiles/opaque.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/stylemenu.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/modal.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/footer.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="../CSSfiles/packbackground.css" />

<style>


#txtother{
color: #030;
text-align:justify;
font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif; }

</style>

</head>

<body>

<ul class="cb-slideshowp" ><!--background image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span></li>
	<li style="list-style-type:none"><span>Image 02</span></li>
    <li style="list-style-type:none"><span>Image 03</span></li>
    <li style="list-style-type:none"><span>Image 04</span></li>
    <li style="list-style-type:none"><span>Image 05</span></li>
    <li style="list-style-type:none"><span>Image 06</span></li>
    
</ul> 

<table width=100%><!--table made to include opaque box logo and heading-->
  
  <tr>
    <td align="center">
    <?php include('session.php'); ?>
     <?php include('header.php'); ?>
    </td>
  </tr>
  
  <tr>
    <td>
     <ul class="menu">
      	<li><a href="home1.php"><font size="+1">Home</font></a></li>
  		<li><a href="accomodation1.php"><font size="+1">Accomodation</font></a></li>
  		<li><a href="dining1.php"><font size="+1">Dining</font></a></li>
  		<li><a href="packages1.php"><font size="+1">Packages</font></a></li>
  		<li><a href="location1.php"><font size="+1">Location</font></a></li>
  		<li id="contact_btncp"><a href="#contactus"><font size="+1">Contact Us</font></a></li>
  		<li><a href="home1.php"><font size="+1">About Us</font></a></li>
  		<li id="booknow_btnp" style="float:right"><a class="active" href="#booknow"><font size="+1">Book Now</font></a></li>
        </ul>
    </td>
  </tr>
  
</table>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<?php
//include('session.php');
echo '<ul class="opaquea" style="text-align:right">
	
	<li style="list-style-type:none;float:right;"><a href="Logout.php">Logout</a></li>
    <li style="list-style-type:none;float:right">'.$login_session.' &nbsp;</li>
</ul>';
?>

<div style="border-bottom-color:#F00; border-bottom:groove;">
<p><h1 id="txtother" style="font-size:48px; ">Tour Packages</h1></p>
<p id="txtother" style="text-align:justify">Enjoy site seeing the amazing place Dover, taken for a tour from our Hotel Happy Holiday. The packages include visits to the wondrous places of Dover. To the Dover museum, to the wild life wingham park to the kearnsney abbey botanical garden and site see the beautiful Dover town in Kent and much more. We will provide you with transport and buffet will be following where you can enjoy your meals at our partner restaurant nearby the location and a guide whom will guide you throughout the tour. All these inclusive in the packages for a reasonable rate.
&nbsp;

	
</p>
</div>

<div>
<div class="changesecp">
<p><h1 id="txtother">Dover Museum</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Packages/dover_museum.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-top">We take you to a tour to the Dover Museum where you can experience the ancient british tradition and culture and the amazing ancient artifacts. A guide along with you would guide you through out the tour and also we will arrange you buffet for lunch from our partners Pacifika kitchen where you will no need to worry about the meals and have a good one
        
    </p>
    
    <p id="txtother" style="font-size:14px">Leaving: 11.00a.m<br />Be back: 5.00p.m<br />Days: Every Mondays and Fridays
   
    </p>
    <p id="txtother" style="font-size:16px">90$ per day (All inclusive)</p>
    </td>
  </tr>
</table>

</p>
</div>

<div class="changesecp">
<p><h1 id="txtother">Kearnsney Abbey</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Packages/kearnsney_abbey.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-top">We take you to a tour to the Kearnsney Abbey botanical garden where you can watch enjoy the beauty of nature. A guide along with you would guide you through out the tour and also we will arrange you buffet for lunch from our partners Pacifika kitchen where you will no need to worry about the meals and have a good one
        
    </p>
    
    <p id="txtother" style="font-size:14px">Leaving: 11.00a.m<br />Be back: 5.00p.m<br />Days: Every Tuesdays and Thursdays
   
    </p>
    <p id="txtother" style="font-size:16px">120$ per day (All inclusive)</p>
    </td>
  </tr>
</table>

</p>
</div>

<div class="changesecp">
<p><h1 id="txtother">Wingham Wild Life Park</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <img src="../Images/Packages/wingham_park.jpg" width="100%"   />
    
    </td>
    <td width="50%"><p id="txtother" style="font-size:16px; vertical-align:text-top">We take you to a tour to the Wingham Wildlife park where you can watch enjoy and take photos with the animals. A guide along with you would guide you through out the tour and also we will arrange you buffet for lunch from our partners Pacifika kitchen where you will no need to worry about the meals and have a good one
        
    </p>
    
    <p id="txtother" style="font-size:14px">Leaving: 11.00a.m<br />Be back: 5.00p.m<br />Days: Every Wednesdays and Saturdays
   
    </p>
    <p id="txtother" style="font-size:16px">140$ per day (All inclusive)</p>
    </td>
  </tr>
</table>

</p>
</div>

<p style="text-align:center">Previous<button onclick="plusDivsp(-1)">&#10094;
</button>
<button onclick="plusDivsp(1)">&#10095;</button>Next</p>

<script>
var slideIndexp = 1;
showDivsp(slideIndexp);

function plusDivsp(j) {
  showDivsp(slideIndexp += j);
}

function showDivsp(j) {
  var k;
  var r = document.getElementsByClassName("changesecp");
  if (j > r.length) {slideIndexp = 1}    
  if (j < 1) {slideIndexp = r.length}
  for (k = 0; k < r.length; k++) {
     r[k].style.display = "none";  
  }
  r[slideIndexp-1].style.display = "block";  
}
</script>

</div>

<div>

<?php include('contactmodalp.php');?>
<?php include('booknowmodalp.php');?>

</div>

<?php include('footer.php'); ?>

</body>
</html>
