<divm id="registerl" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closer">&times;</span>
    <form name="register_forml" method="post" action="" onSubmit="return validate_registerl();">
    <h1 style="text-align:center">Register</h1>
<p style="font-size:18px">First Name</p>
<input name="txt_firstnamerl" type="text" id="txt_firstnamerl" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="txt_lastnamerl" type="text" id="txt_lastnamerl" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="txt_emailrl" type="text" id="txt_emailrl" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="txt_mobilerl" type="text" id="txt_mobilerl" placeholder="Your mobile No." />
<p style="font-size:18px">Username</p>
<input name="txt_usernamerl" type="text" id="txt_usernamerl" placeholder="Create a username" />
<p style="font-size:18px">Password</p>
<input name="txt_passwordrl" type="password" id="txt_passwordrl" placeholder="Enter a password" />
<p style="font-size:18px">Re-enter Password</p>
<input name="txt_repasswordrl" type="text" id="txt_repasswordrl" placeholder="Re-enter the password" />

<p>
  <input type="submit" name="register_btn_submitrl" id="register_btn_submitrl" value="Submit" />
</p>
</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var registermodall = document.getElementById('registerl');

// Get the button that opens the modal
var registerbtnl = document.getElementById("register_btnl");

// Get the <span> element that closes the modal
var registerspanl = document.getElementsByClassName("closer")[0];

// When the user clicks the button, open the modal 
registerbtnl.onclick = function() {
    registermodall.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
registerspanl.onclick = function() {
    registermodall.style.display = "none";
}

function validate_registerl()
{
	var firstnamerl=document.register_forml.txt_firstnamerl;
	var lastnamerl=document.register_forml.txt_lastnamerl;
	var emailrl=document.register_forml.txt_emailrl;
	var contactnorl=document.register_forml.txt_mobilerl;
	var usernamerl=document.register_forml.txt_usernamerl;
	var passwordrl=document.register_forml.txt_passwordrl;
	var repasswordrl=document.register_forml.txt_repasswordrl;
	
	if(EmptyValidationregisterl(firstnamerl,lastnamerl,emailrl,contactnorl,usernamerl,passwordrl,repasswordrl))
	{
		if(AllLettersregisterl(firstnamerl,lastnamerl))
		{
			if(Emailregisterl(emailrl))
			{
				if(Allnumericregisterl(contactnorl))
				{
					if(passwordrl.getText().toString().equals(repasswordrl))
					{
						alert("You have successfully registered");
							return true;
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationregisterl(firstnamerl,lastnamerl,emailrl,contactnorl,usernamerl,passwordrl,repasswordrl)
{
	var firstname_lengthrl=firstnamerl.value.length;
	var lastname_lengthrl=lastnamerl.value.length;
	var email_lengthrl=emailrl.value.length;
	var contactno_lengthrl=contactnorl.value.length;
	var username_lengthrl=usernamerl.value.length;
	var password_lengthrl=passwordrl.value.length;
	var repassword_lengthrl=repasswordrl.value.length;
	
	if(firstname_lengthrl==0||lastname_lengthrl==0||email_lengthrl==0||contactno_lengthrl==0||username_lengthrl==0||password_lengthrl==0||repassword_lengthrl==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersregisterl(firstnamebrl,lastnamerl)
{
	var lettersrl=/^[A-Za-z]+$/;
	if(firstnamerl.value.match(lettersrl)&&lastnamerl.value.match(lettersrl))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		firstnamerl.focus();
		return false;
	}
}


function Emailregisterl(emailrl)
{
	var letterrl=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailrl.value.match(letterrl))
	{

		return true;
	}
	else
	{
		alert("Invalid email format")
		emailrl.focus();
		return false;
	}
}

function Allnumericregisterl(contactnorl)
{
	var lettersnrl=/^[0-9]+$/;
	if(contactnorl.value.match(lettersnrl))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobilerl.focus();
		return false;
	}
}




</script>

<?php
if(isset($_POST['register_btn_submitrl']))
{
include('connection.php');

$firstnamerl=$_POST['txt_firstnamerl'];
$lastnamerl=$_POST['txt_lastnamerl'];
$emailrl=$_POST['txt_emailrl'];
$mobilerl=$_POST['txt_mobilerl'];
$usernamerl=$_POST['txt_usernamerl'];
$passwordrl=$_POST['txt_passwordrl'];

$checkrl="select * from tbl_userdetails where username='$usernamerl'";
$rsl=mysqli_query($conn,$checkrl);
$data1rl=mysqli_fetch_array($rsl, MYSQLI_NUM);
if($data1rl[0]>1)
{
	echo "User already exists</br>";
}
else
{
$sqlrl="insert into tbl_userdetails values('$firstnamerl','$lastnamerl','$emailrl','$mobilerl','$usernamerl','$passwordrl')";
$datarl=mysqli_query($conn,$sqlrl);
if($datarl)
{
echo "You have successfully registered";
}
else
{
die('could not enter data'.mysqli_error());
}
}

}
?>
