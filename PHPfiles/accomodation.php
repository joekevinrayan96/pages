<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hotel Happy Holiday - Accomodation</title>
<link rel="stylesheet" type="text/css" href="../CSSfiles/opaque.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/stylemenu.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/accombackground.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/modal.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/footer.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<style>

#txtother{
color: #030;
text-align:justify;
font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif; }

</style>

</head>

<body>

<ul class="cb-slideshowa" ><!--background image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span></li>
	<li style="list-style-type:none"><span>Image 02</span></li>
    <li style="list-style-type:none"><span>Image 03</span></li>
    <li style="list-style-type:none"><span>Image 04</span></li>
    <li style="list-style-type:none"><span>Image 05</span></li>
    <li style="list-style-type:none"><span>Image 06</span></li>
    
</ul> 

<table width=100%>
  <tr>
    <td align="center">
     <?php include('header.php'); ?>
    </td>
  </tr>
  
  <tr>
    <td>
     <ul class="menu">
      	<li><a href="home.php"><font size="+1">Home</font></a></li>
  		<li><a href="accomodation.php"><font size="+1">Accomodation</font></a></li>
  		<li><a href="dining.php"><font size="+1">Dining</font></a></li>
  		<li><a href="packages.php"><font size="+1">Packages</font></a></li>
  		<li><a href="location.php"><font size="+1">Location</font></a></li>
  		<li id="login_btnca"><a href="#contactus"><font size="+1">Contact Us</font></a></li>
  		<li><a href="home.php"><font size="+1">About Us</font></a></li>
  		<li id="login_btnba" style="float:right"><a class="active" href="#booknow"><font size="+1">Book Now</font></a></li>
        </ul>
    </td>
  </tr>
  
</table>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<ul class="opaquea" style="text-align:right">
	
	<li id="login_btna" style="list-style-type:none;float:right;"><font color="#FFFFFF"><a href="#login">Login</a></font></li>
    <li style="list-style-type:none;float:right"><font color="#FFFFFF">Guest &nbsp;</font></li>
</ul>

<div style="border-bottom-color:#F00; border-bottom:groove;">
<p><h1 id="txtother" style="font-size:48px; ">Accomodation</h1></p>
<p id="txtother" style="text-align:justify">Retreat to one of our 221 private guest-rooms or suites just steps away from the dazzling white sandy beach. All of our accommodations include free wi-fi, coffeemakers, mini-fridges, cable TV, air conditioning, direct dial phone, in-room safe, hairdryer, quality bathroom amenities and private balcony. Luxury suites feature jacuzzi tubs and living areas with the added option of a private bar.
&nbsp;
<br />
<br />
	
Check in: 12.00pm | Check out: 3.00pm
</p>
</div>
<div>
<div class="changesec">
<p><h1 id="txtother">Island View</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <p id="txtother">Rooms are equipped with a king size bed and can accomodate a maximum number of 3 persons.
    <br />
    Note that the room will be initially available only for 2 persons and on request with no charge arrangements can be made for the third person.
    </p>
    <p id="txtother" style="font-size:18px">Additional Amenities
    <ul id="txtother">
    <li>Private balcony</li>
    <li>Mini-size refrigirator</li>
    <li>Private locker</li>
    <li>Television</li>
    <li>Cupboard</li>
    </ul>
    </p>
    <p id="txtother" style="font-size:24px;">&nbsp;   $129 per night</p>
    </td>
    <td width="50%"><img src="../Images/Accomodation/island_view.jpg" width="100%"   /></td>
  </tr>
</table>

</p>
</div>
<div class="changesec">
<p><h1 id="txtother">Ocean View</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <p id="txtother">A luxury room equipped with two king size beds and can accomodate a maximum number of 4 persons.
    
    
    </p>
    <p id="txtother" style="font-size:18px">Additional Amenities
    <ul id="txtother">
    <li>Private balcony</li>
    <li>Mini-size refrigirator</li>
    <li>Private locker</li>
    <li>Television</li>
    <li>Cupboard</li>
    
    </ul>
    </p>
    <p id="txtother" style="font-size:24px;">&nbsp;   $189 per night</p>
    </td>
    <td width="50%"><img src="../Images/Accomodation/Ocean_view.jpg" width="100%"   />&nbsp;  </td>
    
  </tr>
  
</table>

</p>
</div>

<div class="changesec">

<p><h1 id="txtother">Pool View</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <p id="txtother">A luxury room equipped with three king size beds and can accomodate a maximum number of 6 persons.
    
    
    </p>
    <p id="txtother" style="font-size:18px">Additional Amenities
    <ul id="txtother">
    <li>Private balcony</li>
    <li>Mini-size refrigirator</li>
    <li>Private locker</li>
    <li>Television</li>
    <li>Cupboard</li>
    <li>Interconnecting rooms</li>
    <li>Private bar</li>
    </ul>
    </p>
    
    <p id="txtother" style="font-size:24px;">&nbsp;   $240 per night</p>
    </td>
    <td width="50%"><img src="../Images/Accomodation/Pool_View_Double.jpg" width="100%"   />&nbsp;  </td>
    
  </tr>
  
</table>

</p>
</div>

<div class="changesec">
<p><h1 id="txtother">State Room</h1>
<table width="100%" style="border-bottom-color:#F00; border-bottom:groove;">
  <tr>
    <td width="50%">
    <p id="txtother">Rooms are equipped with a bed and can accomodate only upto 2 persons.
    
    
    </p>
    <p id="txtother" style="font-size:18px">Additional Amenities
    <ul id="txtother">
    <li>Mini-size refrigirator</li>
    <li>Private locker</li>
    <li>Television</li>
    <li>Cupboard</li>
    </ul>
    </p>
    <p id="txtother" style="font-size:24px;">&nbsp;   $60 per night</p>
    </td>
    <td width="50%"><img src="../Images/Accomodation/State_room.jpg" width="100%"   />&nbsp;</td>
  </tr>
</table>

</p>
</div>

<p style="text-align:center">Previous<button onclick="plusDivsa(-1)">&#10094;</button>
<button onclick="plusDivsa(1)">&#10095;</button>Next</p>

<script>
var slideIndexa = 1;
showDivsa(slideIndexa);

function plusDivsa(n) {
  showDivsa(slideIndexa += n);
}

function showDivsa(n) {
  var i;
  var x = document.getElementsByClassName("changesec");
  if (n > x.length) {slideIndexa = 1}    
  if (n < 1) {slideIndexa = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndexa-1].style.display = "block";  
}
</script>

</div>

<p id="txtother" style="font-size:18px">For quick reservation please call 0112451932</p>

<div>

<?php include('loginmodala.php');?>
<?php include('registermodala.php');?>

</div>

<?php include('footer.php'); ?>

</body>
</html>
