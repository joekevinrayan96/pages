<?php include('session.php'); ?>
<divm id="booknowl" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closeb">&times;</span>
    <form name="booknow_forml" method="post" action="home.html" onsubmit="return validate_booknowl();">
    <h1 style="text-align:center">Book Now</h1>
<p style="font-size:18px">First Name</p>
<input name="txt_firstnamebl" type="text" id="txt_firstnamebl" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="txt_lastnamebl" type="text" id="txt_lastnamebl" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="txt_emailbl" type="text" id="txt_emailbl" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="txt_mobilebl" type="text" id="txt_mobilebl" placeholder="Your mobile No." />
<p style="font-size:18px">Check in</p>
<input name="date_checkinbl" type="date" id="date_checkinbl" />
<p style="font-size:18px">Check out</p>
<input name="date_checkoutbl" type="date" id="date_checkoutbl" />
<p style="font-size:18px">Select type of room</p>
<input list="list_roomsbl" name="cmb_roomsibl" id="cmb_roomsibl" type="text"/>
<datalist id="list_roomsbl">
    <option value="Island View">
    <option value="Ocean View">
    <option value="Pool View">
    <option value="State Room">
</datalist>

<p>
  <input type="submit" name="booknow_btn_submitl" id="booknow_btn_submitl" value="Submit" />
</p>
</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var booknowmodall = document.getElementById('booknowl');

// Get the button that opens the modal
var booknowbtnl = document.getElementById("booknow_btnl");

// Get the <span> element that closes the modal
var booknowspanl = document.getElementsByClassName("closel")[0];

// When the user clicks the button, open the modal 
booknowbtnl.onclick = function() {
    booknowmodall.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
booknowspanl.onclick = function() {
    booknowmodall.style.display = "none";
}

function validate_booknowl()
{
	var firstnamebl=document.booknow_forml.txt_firstnamebl;
	var lastnamebl=document.booknow_forml.txt_lastnamebl;
	var emailbl=document.booknow_forml.txt_emailbl;
	var contactnobl=document.booknow_forml.txt_mobilebl;
	var checkinbl=document.booknow_forml.date_checkinbl;
	var checkoutbl=document.booknow_forml.date_checkoutbl;
	var roombl=document.booknow_forml.cmb_roomsibl;
	
	if(EmptyValidationbooknowl(firstnamebl,lastnamebl,emailbl,contactnobl,checkinbl,checkoutbl,roombl))
	{
		if(AllLettersbooknowl(firstnamebl,lastnamebl))
		{
			if(Emailbooknowl(emailbl))
			{
				if(Allnumericbooknowl(contactnobl))
				{
					if(!checkinbl.getText().toString().equals("mm/dd/yyyy"))
					{
						if(!checkoutbl.getText().toString().equals("mm/dd/yyyy"))
						{
							alert("Booking Details successfully sent!");
							return true;
						}
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationbooknowl(firstnamebl,lastnamebl,emailbl,contactnobl,checkinbl,checkoutbl,roombl)
{
	var firstname_lengthbl=firstnamebl.value.length;
	var lastname_lengthbl=lastnamebl.value.length;
	var email_lengthbl=emailbl.value.length;
	var contactno_lengthbl=contactnobl.value.length;
	var checkin_lengthbl=checkinbl.value.length;
	var checkout_lengthbl=checkoutbl.value.length;
	var room_lengthbl=roombl.value.length;
	
	if(firstname_lengthbl==0||lastname_lengthbl==0||email_lengthbl==0||contactno_lengthbl==0||checkin_lengthbl==0||checkout_lengthbl==0||room_lengthbl==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersbooknowl(firstnamebl,lastnamebl)
{
	var lettersbl=/^[A-Za-z]+$/;
	if(firstnamebl.value.match(lettersbl)&&lastnamebl.value.match(lettersbl))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		firstnamebl.focus();
		return false;
	}
}



function Emailbooknowl(emailbl)
{
	var letterbl=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailbl.value.match(letterbl))
	{
		return true;
	}
	else
	{
		alert("Invalid email format")
		emailbl.focus();
		return false;
	}
}

function Allnumericbooknowl(contactnobl)
{
	var lettersnbl=/^[0-9]+$/;
	if(contactnobl.value.match(lettersnbl))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobilebl.focus();
		return false;
	}
}

</script>

<?php
if(isset($_POST['booknow_btn_submitl']))
{
include('connection.php');

$firstnamebl=$_POST['txt_firstnamebl'];
$lastnamebl=$_POST['txt_lastnamebl'];
$emailbl=$_POST['txt_emailbl'];
$mobilebl=$_POST['txt_mobilebl'];
$checkinbl=$_POST['date_checkinbl'];
$checkoutbl=$_POST['date_checkoutbl'];
$roombl=$_POST['cmb_roomsibl'];

$sqlbl="insert into tbl_bookdetails values('$firstnamebl','$lastnamebl','$emailbl','$mobilebl','$checkinbl','$checkoutbl','$roombl','$login_session')";
$databl=mysqli_query($conn,$sqlbl);
if($databl)
{
echo "You bookings have been saved! Soon we will contact you! Thank you!";
}
else
{
die('could not enter data'.mysqli_error());
}
}

?>
