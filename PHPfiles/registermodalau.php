<divm id="registerau" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closer">&times;</span>
    <form name="register_formau" method="post" action="" onsubmit="return validate_registerau();">
    <h1 style="text-align:center">Register</h1>
<p style="font-size:18px">First Name</p>
<input name="txt_firstnamerau" type="text" id="txt_firstnamerau" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="txt_lastnamerau" type="text" id="txt_lastnamerau" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="txt_emailrau" type="text" id="txt_emailrau" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="txt_mobilerau" type="text" id="txt_mobilerau" placeholder="Your mobile No." />
<p style="font-size:18px">Username</p>
<input name="txt_usernamerau" type="text" id="txt_usernamerau" placeholder="Create a username" />
<p style="font-size:18px">Password</p>
<input name="txt_passwordrau" type="password" id="txt_passwordrau" placeholder="Enter a password" />
<p style="font-size:18px">Re-enter Password</p>
<input name="txt_repasswordrau" type="text" id="txt_repasswordrau" placeholder="Re-enter the password" />

<p>
  <input type="submit" name="register_btn_submitrau" id="register_btn_submitrau" value="Submit" />
</p>
</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var registermodalau = document.getElementById('registerau');

// Get the button that opens the modal
var registerbtnau = document.getElementById("register_btnau");

// Get the <span> element that closes the modal
var registerspanau = document.getElementsByClassName("closer")[0];

// When the user clicks the button, open the modal 
registerbtnau.onclick = function() {
    registermodalau.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
registerspanau.onclick = function() {
    registermodalau.style.display = "none";
}

function validate_registerau()
{
	var firstnamerau=document.register_formau.txt_firstnamerau;
	var lastnamerau=document.register_formau.txt_lastnamerau;
	var emailrau=document.register_formau.txt_emailrau;
	var contactnorau=document.register_formau.txt_mobilerau;
	var usernamerau=document.register_formau.txt_usernamerau;
	var passwordrau=document.register_formau.txt_passwordrau;
	var repasswordrau=document.register_formau.txt_repasswordrau;
	
	if(EmptyValidationregisterau(firstnamerau,lastnamerau,emailrau,contactnorau,usernamerau,passwordrau,repasswordrau))
	{
		if(AllLettersregisterau(firstnamerau,lastnamerau))
		{
			if(Emailregisterau(emailrau))
			{
				if(Allnumericregisterau(contactnorau))
				{
					if(passwordrau.getText().toString().equals(repasswordrau))
					{
						alert("You have successfully registered");
							return true;
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationregisterau(firstnamerau,lastnamerau,emailrau,contactnorau,usernamerau,passwordrau,repasswordrau)
{
	var firstname_lengthrau=firstnamerau.value.length;
	var lastname_lengthrau=lastnamerau.value.length;
	var email_lengthrau=emailrau.value.length;
	var contactno_lengthrau=contactnorau.value.length;
	var username_lengthrau=usernamerau.value.length;
	var password_lengthrau=passwordrau.value.length;
	var repassword_lengthrau=repasswordrau.value.length;
	
	if(firstname_lengthrau==0||lastname_lengthrau==0||email_lengthrau==0||contactno_lengthrau==0||username_lengthrau==0||password_lengthrau==0||repassword_lengthrau==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersregisterau(firstnamebrau,lastnamerau)
{
	var lettersrau=/^[A-Za-z]+$/;
	if(firstnamerau.value.match(lettersrau)&&lastnamerau.value.match(lettersrau))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		firstnamerau.focus();
		return false;
	}
}


function Emailregisterau(emailrau)
{
	var letterrau=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailrau.value.match(letterrau))
	{

		return true;
	}
	else
	{
		alert("Invalid email format")
		emailrau.focus();
		return false;
	}
}

function Allnumericregisterau(contactnorau)
{
	var lettersnrau=/^[0-9]+$/;
	if(contactnorau.value.match(lettersnrau))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobilerau.focus();
		return false;
	}
}




</script>

<?php
if(isset($_POST['register_btn_submitrau']))
{
include('connection.php');

$firstnamerau=$_POST['txt_firstnamerau'];
$lastnamerau=$_POST['txt_lastnamerau'];
$emailrau=$_POST['txt_emailrau'];
$mobilerau=$_POST['txt_mobilerau'];
$usernamerau=$_POST['txt_usernamerau'];
$passwordrau=$_POST['txt_passwordrau'];

$sqlrau="insert into tbl_userdetails values('$firstnamerau','$lastnamerau','$emailrau','$mobilerau','$usernamerau','$passwordrau')";
$datarau=mysqli_query($conn,$sqlrau);
if($datarau)
{
echo "You have successfully registered";
}
else
{
die('could not enter data'.mysqli_error());
}
}
else
{
echo "Submit the  form first";
}
?>
