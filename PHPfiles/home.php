<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hotel Happy Holiday - Home</title>

<link rel="stylesheet" type="text/css" href="../CSSfiles/stylebackground.css"/>
<link rel="stylesheet" type="text/css" href="../CSSfiles/stylemenu.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="../CSSfiles/modal.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/footer.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/opaque.css" />
<link rel="stylesheet" type="text/css" href="../CSSfiles/slideanim.css"/>
<link rel="stylesheet" type="text/css" href="../CSSfiles/log.css"/>

<style>

#txtother{
color: #030;
text-align:justify;
font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif; }
	
#txtblbgimg{
color: #030;
text-align:center;
font-family:"Palatino Linotype", "Book Antiqua", Palatino, serif; }

#slide{
width:200px;
height:150px;
min-height:80px;
min-width:120px; }

.txt-inbetween{
	font-size:18px;
	color:white;
	position:absolute;
	overflow:hidden; }

</style>

</head>

<body>

<ul class="cb-slideshow" ><!--background image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span></li>
	<li style="list-style-type:none"><span>Image 02</span></li>
    <li style="list-style-type:none"><span>Image 03</span></li>
    <li style="list-style-type:none"><span>Image 04</span></li>
    <li style="list-style-type:none"><span>Image 05</span></li>
    <li style="list-style-type:none"><span>Image 06</span></li>
</ul> 

<table width=100%>
  <tr>
    <td align="center">
     <?php include('header.php'); ?>
    </td>
  </tr>
  
  <tr>
    <td>
    <ul class="menu">
      	<li><a href="home.php"><font size="+1">Home</font></a></li>
  		<li><a href="accomodation.php"><font size="+1">Accomodation</font></a></li>
  		<li><a href="dining.php"><font size="+1">Dining</font></a></li>
  		<li><a href="packages.php"><font size="+1">Packages</font></a></li>
  		<li><a href="location.php"><font size="+1">Location</font></a></li>
  		<li id="login_btnch"><a href="#contactus"><font size="+1">Contact Us</font></a></li>
  		<li><a href="home.php"><font size="+1">About Us</font></a></li>
  		<li id="login_btnbh" style="float:right"><a class="active" href="#booknow"><font size="+1">Book Now</font></a></li>
        </ul>
        
    </td>
  </tr>
  
</table>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<ul class="opaquea" style="text-align:right">
	
	<li id="login_btnh" style="list-style-type:none;float:right;"><font color="#FFFFFF"><a href="#login">Login</a></font></li>
    <li style="list-style-type:none;float:right"><font color="#FFFFFF">Guest &nbsp;</font></li>
</ul>

<br />

 <div>
 <section id="aboutus">
  <p> <h1 id="txtblbgimg"> Hotel Happy Holiday </h1> </p>
  <p id="txtblbgimg" style="font-size: 18px">Enjoy your holiday at the iconic Hotel Happy Holiday at the Dover beach as this beachfront property encompasses all the charm, colour and vibrancy of an idyllic paradise holiday. With the beach literaly at your doorstep you can watch the waves dance, feel the warm golden sand beneath your feet and be cooled by the breeze as the sun gently warms your skin. Hotel happy holiday delivers the quality catering to the needs of guests so that each and every guest leaves with fond memories. Leave your cares behind and come on over to our hotel, where experience after experience awaits you!
  </p>
  </section>
 </div>
  
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
   
 <table width="100%">
   <tr>
     <td width="50%"><p><h1 id="txtblbgimg" style="text-align:left">Accomodation</h1></p><p id="txtother" style="font-size:16px">Retreat to one of our 221 private guest-rooms or suites just steps away from the dazzling white sandy beach. All of our accommodations include free wi-fi, coffeemakers, mini-fridges, cable TV, air conditioning, direct dial phone, in-room safe, hairdryer, quality bathroom amenities and private balcony. Luxury suites feature jacuzzi tubs and living areas with the added option of a private bar.</p></td>
     <td width="50%">
        
        <ul class="slide-anima" ><!--image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span><div><h4>Island View</h4></div></li>
	<li style="list-style-type:none"><span>Image 02</span><div><h4>Ocean View</h4></div></li>
    <li style="list-style-type:none"><span>Image 03</span><div><h4>Pool View</h4></div></li>
    <li style="list-style-type:none"><span>Image 04</span><div><h4>State Room</h4></div></li>
    <li style="list-style-type:none"><span>Image 05</span><div><h4>Island View</h4></div></li>
    <li style="list-style-type:none"><span>Image 06</span><div><h4>Pool View</h4></div></li>
    
 </ul>
       </td>
     </tr>
   </table>
   
   <br />
   <table width="100%">
     <tr>
       <td width="50%">
       <ul class="slide-animd" ><!--image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span><div><h4>Accra Deck</h4></div></li>
	<li style="list-style-type:none"><span>Image 02</span><div><h4>Coco Patch</h4></div></li>
    <li style="list-style-type:none"><span>Image 03</span><div><h4>Fig Tree</h4></div></li>
    <li style="list-style-type:none"><span>Image 04</span><div><h4>Lagoon Bar</h4></div></li>
    <li style="list-style-type:none"><span>Image 05</span><div><h4>Pacifica Kitchen</h4></div></li>
    <li style="list-style-type:none"><span>Image 06</span><div><h4>Sand Bar</h4></div></li>
    
 </ul>
</td>
	   <td width="50%"><p><h1 id="txtblbgimg" style="text-align:left">Dining</h1></p>
	    <p id="txtother" style="font-size:16px">
	   Enjoy an exquisite dining experience featuring local and contemporary European cuisine at Accra Deck Restaurant – a picturesque venue with outdoor seating overlooking the ocean. Get your taste buds tingling with delights of the Pacific Rim at our Pacifika Kitchen Restaurant featuring Japanese, Chinese, Thai and Indonesian cuisine. For something more casual, head to the CocoPatch restaurant for a hearty breakfast or lunch. On Tuesdays and Thursdays, immerse yourself in the local culture at our specialty dinners with live music and entertainment at The Fig Tree Restaurant. Thirsty? Grab an ice cold drink at the Lagoon Bar, our swim-up pool bar. This laid-back hangout has an enticing snack bar menu for poolside bites and hosts karaoke every Wednesday evening. We also have the Sand Bar, an indoor bar/lounge with a big-screen TV for watching your favorite sports games or enjoy cocktails as the sun set at our newly established Sunset Bar. Don’t feel like coming down to dine? Our in room dining is available daily from 7.00 a.m. to 11.00 p.m.
	    </p>
	   </td>
     </tr>
   </table>
   <br />
   <table width="100%">
     <tr>
       <td width="50%"><p><h1 id="txtblbgimg" style="text-align:left">Tour Packages</h1></p>
       <p id="txtother" style="font-size:16px">
       Enjoy site seeing the amazing place Dover, taken for a tour from our Hotel Happy Holiday. The packages include visits to the wondrous places of Dover. To the Dover museum, to the wild life wingham park to the kearnsney abbey botanical garden and site see the beautiful Dover town in Kent and much more. We will provide you with transport and buffet will be following where you can enjoy your meals at our partner restaurant nearby the location and a guide whom will guide you throughout the tour. All these inclusive 
in the packages for a reasonable rate.
       </p>
       </td>
       <td width="50%">
      <ul class="slide-animp" ><!--image slideshow-->
	<li style="list-style-type:none"><span>Image 01</span><div><h4>Dover Museum</h4></div></li>
	<li style="list-style-type:none"><span>Image 02</span><div><h4>Kearnsney Abbey</h4></div></li>
    <li style="list-style-type:none"><span>Image 03</span><div><h4>Wingham Park</h4></div></li>
    <li style="list-style-type:none"><span>Image 04</span><div><h4>Dover Museum</h4></div></li>
    <li style="list-style-type:none"><span>Image 05</span><div><h4>Kearnsney Abbey</h4></div></li>
    <li style="list-style-type:none"><span>Image 06</span><div><h4>Wingham Park</h4></div></li>
    </ul>
       </td>
     </tr>
   </table>
   <p>&nbsp;</p>
   
   
   
<div>

<?php include('loginmodalh.php');?>
<?php include('registermodalh.php');?>

</div>
   
<?php include('footer.php'); ?>
     
</body>

</html>
