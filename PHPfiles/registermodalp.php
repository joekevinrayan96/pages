<divm id="registerp" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closer">&times;</span>
    <form name="register_formp" method="post" action="" onsubmit="return validate_registerp();">
    <h1 style="text-align:center">Register</h1>
<p style="font-size:18px">First Name</p>
<input name="txt_firstnamerp" type="text" id="txt_firstnamerp" placeholder="Your First Name"/>
<p style="font-size:18px">Last Name</p>
<input name="txt_lastnamerp" type="text" id="txt_lastnamerp" placeholder="Your Last Name"/>
<p style="font-size:18px">Email</p>
<input name="txt_emailrp" type="text" id="txt_emailrp" placeholder="Your email address"/>
<p style="font-size:18px">Contact No</p>
<input name="txt_mobilerp" type="text" id="txt_mobilerp" placeholder="Your mobile No." />
<p style="font-size:18px">Username</p>
<input name="txt_usernamerp" type="text" id="txt_usernamerp" placeholder="Create a username" />
<p style="font-size:18px">Password</p>
<input name="txt_passwordrp" type="password" id="txt_passwordrp" placeholder="Enter a password" />
<p style="font-size:18px">Re-enter Password</p>
<input name="txt_repasswordrp" type="text" id="txt_repasswordrp" placeholder="Re-enter the password" />

<p>
  <input type="submit" name="register_btn_submitrp" id="register_btn_submitrp" value="Submit" />
</p>
</form>
  </div>

</divm>

<script type="text/javascript">
// Get the modal
var registermodalp = document.getElementById('registerp');

// Get the button that opens the modal
var registerbtnp = document.getElementById("register_btnp");

// Get the <span> element that closes the modal
var registerspanp = document.getElementsByClassName("closer")[0];

// When the user clicks the button, open the modal 
registerbtnp.onclick = function() {
    registermodalp.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
registerspanp.onclick = function() {
    registermodalp.style.display = "none";
}

function validate_registerp()
{
	var firstnamerp=document.register_formp.txt_firstnamerp;
	var lastnamerp=document.register_formp.txt_lastnamerp;
	var emailrp=document.register_formp.txt_emailrp;
	var contactnorp=document.register_formp.txt_mobilerp;
	var usernamerp=document.register_formp.txt_usernamerp;
	var passwordrp=document.register_formp.txt_passwordrp;
	var repasswordrp=document.register_formp.txt_repasswordrp;
	
	if(EmptyValidationregisterp(firstnamerp,lastnamerp,emailrp,contactnorp,usernamerp,passwordrp,repasswordrp))
	{
		if(AllLettersregisterp(firstnamerp,lastnamerp))
		{
			if(Emailregisterp(emailrp))
			{
				if(Allnumericregisterp(contactnorp))
				{
					if(passwordrp.getText().toString().equals(repasswordrp))
					{
						alert("You have successfully registered");
							return true;
					}
				}
			}
		}
		
	}
	return false;
	
	
}

function EmptyValidationregisterp(firstnamerp,lastnamerp,emailrp,contactnorp,usernamerp,passwordrp,repasswordrp)
{
	var firstname_lengthrp=firstnamerp.value.length;
	var lastname_lengthrp=lastnamerp.value.length;
	var email_lengthrp=emailrp.value.length;
	var contactno_lengthrp=contactnorp.value.length;
	var username_lengthrp=usernamerp.value.length;
	var password_lengthrp=passwordrp.value.length;
	var repassword_lengthrp=repasswordrp.value.length;
	
	if(firstname_lengthrp==0||lastname_lengthrp==0||email_lengthrp==0||contactno_lengthrp==0||username_lengthrp==0||password_lengthrp==0||repassword_lengthrp==0)
	{
		alert("Fields should not be empty");
		return false;
			
	}
	else
	{
		return true;
	}
	
}

function AllLettersregisterp(firstnamebrp,lastnamerp)
{
	var lettersrp=/^[A-Za-z]+$/;
	if(firstnamerp.value.match(lettersrp)&&lastnamerp.value.match(lettersrp))
	{
		return true;
	}
	else
	{
		alert('Firstname and Lastname should contain only alphabets');
		firstnamerp.focus();
		return false;
	}
}


function Emailregisterp(emailrp)
{
	var letterrp=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(emailrp.value.match(letterrp))
	{

		return true;
	}
	else
	{
		alert("Invalid email format")
		emailrp.focus();
		return false;
	}
}

function Allnumericregisterp(contactnorp)
{
	var lettersnrp=/^[0-9]+$/;
	if(contactnorp.value.match(lettersnrp))
	{
		return true;
	}
	else
	{
		alert("mobile no should contain only numbers");
		mobilerp.focus();
		return false;
	}
}




</script>

<?php
if(isset($_POST['register_btn_submitrp']))
{
include('connection.php');

$firstnamerp=$_POST['txt_firstnamerp'];
$lastnamerp=$_POST['txt_lastnamerp'];
$emailrp=$_POST['txt_emailrp'];
$mobilerp=$_POST['txt_mobilerp'];
$usernamerp=$_POST['txt_usernamerp'];
$passwordrp=$_POST['txt_passwordrp'];

$checkrp="select * from tbl_userdetails where username='$usernamerp'";
$rsp=mysqli_query($conn,$checkrp);
$data1rp=mysqli_fetch_array($rsp, MYSQLI_NUM);
if($data1rp[0]>1)
{
	echo "User already exists</br>";
}
else
{
$sqlrp="insert into tbl_userdetails values('$firstnamerp','$lastnamerp','$emailrp','$mobilerp','$usernamerp','$passwordrp')";
$datarp=mysqli_query($conn,$sqlrp);
if($datarp)
{
echo "You have successfully registered";
}
else
{
die('could not enter data'.mysqli_error());
}
}

}
?>
